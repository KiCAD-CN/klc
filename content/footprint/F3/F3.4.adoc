+++
title = "F3.4 SMD 贴片 IC 封装命名参考规范"
+++

{{< fp_naming_header type="SMD IC package" >}}

== 鸥翼型封装 Gullwing Packages

下面提供了针对鸥翼型封装 (Gullwing Package) 的命名格式参考。

示例:

* SOIC
* SOP
* QFP
* J-Lead


{{< fp_code name="ic_gull" >}}

注意:

. [`PKG`] - 指该封装在行业内最常用的名称。一般会使用 JEDEC (一种常用的半导体封装标准) 中的命名，但也可以根据需要使用其它标准中的名称。
. [`Pin count`] - 拥有_唯一编号_的焊盘数量 (不包括器件下方的裸露焊盘 (散热焊盘))。
. [`Modifiers`] - 该项具体参数因个别 IC 封装类型而异，根据具体情况添加。可能的参数包括:
* Clearance
* 引脚尺寸 Lead size
* 焊盘尺寸 Pad size
* 裸露焊盘 (散热焊盘) 尺寸 Exposed pad size
* 阻焊层延伸量 Soldermask expansion

== 无引脚封装 No-Lead Packages

示例:

* DFN
* QFN
* LCC

{{< fp_code name="ic_dfn" >}}

注意:

. 引脚长度指的该封装针对中等密度等级所设计的引脚长度。
. 如果有需要，可以额外提供引脚宽度。

== 球栅阵列封装 Ball Grid Array Packages

{{< fp_code name="ic_bga" >}}

注意:

. 对于 BGA 封装来说，仅仅提供引脚数量是不够的，还需要提供 横向排列的引脚数 [`Columns`] 和 纵向排列的引脚数 [`Rows`]。
. 一些 BGA 封装的 横向脚距 和 纵向脚距 是不一样的，如果遇到这种情况，两个脚距 [`Pitch`] 都必须提供。
. 必须提供引脚球直径 [`Ball`]。
. [`NSMD/SMD`]
  * NSMD 表示封装是为 `非阻焊层限定焊盘` 而设计的 (阻焊开窗尺寸大于焊盘)。
  * SMD 表示封装是为 `阻焊层限定焊盘` 而设计的 (阻焊开窗尺寸小于焊盘)。
